$(document).ready(function () {
    if ($("#category-table").length)
        $('#category-table').DataTable();

    $(".my-btn-modal-custom-h").on("click", function (event) {
        var idModal = $(this).attr('data-target');
        $('#' + idModal).show();
    });
    $(".close-custom-h").on("click", function (event) {
        var idModal = $(this).attr('data-target');
        $('#' + idModal).hide();
    });
    if ($("#long-description").length) {
        CKEDITOR.replace('longDescription');
        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
        CKEDITOR.config.entities = false;
    }

    if ($("#specification").length) {
        CKEDITOR.replace('specification');
        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
        CKEDITOR.config.entities = false;
    }

    $('#updateRecord').on("click",function (){
        $.ajax({
            url: Routing.generate("update-record", {}),
            type: 'POST',
            async: false,
            data: {
            },
            success: function (response) {
                console.log('success');
                location.reload();
            },
            error: function () {
                console.log('error update record');
            }
        });
    });


});