<?php

declare(strict_types=1);

namespace App\Entity;

use App\Traits\EntityEnableTrait;
use App\Traits\EntityIdTrait;
use App\Traits\EntityTimestamp;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Country.
 *
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 */
class Country
{
    use EntityIdTrait;
    use EntityEnableTrait;
    use EntityTimestamp;


    /**
     * @var string|null
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @var Record|null
     * @ORM\OneToMany(targetEntity = "Record", mappedBy="country", orphanRemoval=true)
     */
    private $record;

    /**
     * Category constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('NOW'));
        $this->record = new ArrayCollection();
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getRecord(): Collection
    {
        return $this->record;
    }

    /**
     * @return $this
     */
    public function removeRecord(Record $record): self
    {
        if (true === $this->record->contains($record)) {
            $this->record->removeElement($record);
            if ($record->getCountry() === $this) {
                $record->setCountry(null);
            }
        }
        return $this;
    }
}
