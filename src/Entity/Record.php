<?php

declare(strict_types=1);

namespace App\Entity;

use App\Traits\EntityEnableTrait;
use App\Traits\EntityIdTrait;
use App\Traits\EntityTimestamp;
use Doctrine\ORM\Mapping as ORM;

/**
 * Record.
 *
 * @ORM\Table(name="record")
 * @ORM\Entity(repositoryClass="App\Repository\RecordRepository")
 */
class Record
{
    use EntityIdTrait;
    use EntityEnableTrait;
    use EntityTimestamp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mcc", type="string", length=255, nullable=true)
     */
    private $Mcc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mnc", type="string", length=255, nullable=true)
     */
    private $Mnc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="network", type="string", length=255, nullable=true)
     */
    private $Network;

    /**
     * @var string|null
     *
     * @ORM\Column(name="operator_or_brand", type="string", length=255, nullable=true)
     */
    private $operatorOrBrand;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var Country|null
     *
     * @ORM\ManyToOne(targetEntity = "Country", inversedBy="record")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    protected $country;

    /**
     * Category constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('NOW'));
    }

    /**
     * @return string|null
     */
    public function getMcc(): ?string
    {
        return $this->Mcc;
    }

    /**
     * @param string|null $Mcc
     * @return Record
     */
    public function setMcc($Mcc): self
    {
        $this->Mcc = $Mcc;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMnc(): ?string
    {
        return $this->Mnc;
    }

    /**
     * @param string|null $Mnc
     * @return Record
     */
    public function setMnc($Mnc): self
    {
        $this->Mnc = $Mnc;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNetwork(): ?string
    {
        return $this->Network;
    }

    /**
     * @param string|null $Network
     * @return Record
     */
    public function setNetwork($Network): self
    {
        $this->Network = $Network;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOperatorOrBrand(): ?string
    {
        return $this->operatorOrBrand;
    }

    /**
     * @param string|null $operatorOrBrand
     * @return Record
     */
    public function setOperatorOrBrand($operatorOrBrand): self
    {
        $this->operatorOrBrand = $operatorOrBrand;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return Record
     */
    public function setStatus($status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param Country|null $country
     * @return Record
     */
    public function setCountry($country): self
    {
        $this->country = $country;
        return $this;
    }

}
