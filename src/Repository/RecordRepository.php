<?php

declare(strict_types=1);

namespace App\Repository;

/**
 * ModelRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RecordRepository extends \Doctrine\ORM\EntityRepository
{
    public function findModelByBrand($brand)
    {
        $qb = $this->createQueryBuilder('p')->select('p.id,p.label');
        $qb->where("p.brand= '".$brand."'");

        return $qb->getQuery()
            ->getResult();
    }
}
