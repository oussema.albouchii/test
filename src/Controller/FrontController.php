<?php

namespace App\Controller;


use App\Entity\Country;
use App\Entity\Record;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FrontController extends Controller
{
    public function index(Request $request)
    {
        $country = $this->getDoctrine()
            ->getRepository(Country::class)
            ->findBy(['enabled' => 1], ['id' => 'ASC']);

        return $this->render('back/product/list.twig', ['countries' => $country
        ]);
    }

    public function updateRecord(Request $request)
    {
        if ($request->isMethod('post')) {
            $client = HttpClient::create();
            $response = $client->request('GET', 'https://cellidfinder.com/mcc-mnc', ['body' => $request->request->all()]);
            $crawler = new Crawler($response->getContent());
            $contents = $crawler->filter('.well')->outerHtml();
            $h3 = $crawler->filter('.well')->filter('h3')->each(function ($h3, $i) {
                return trim($h3->text());
            });
            $table = $crawler->filter('table')->filter('table')->each(function ($oneTable, $i) {
                return trim($oneTable->outerHtml());
            });
            $table2 = [];
            foreach ($table as $singleTable) {
                $crawler2 = new Crawler($singleTable);
                $table2[] = $crawler2->filter('tr')->each(function ($tr, $i) {
                    return $tr->filter('td')->each(function ($td, $i) {
                        return $td->outerHtml();
                    });
                });
            }
            for ($i = 0; $i < count($table2); $i++) {
                if (false === empty($table2[$i])) {
                    $country = new Country();
                    $country->setLabel($h3[$i]);
                    $country = $this->save($country);
                    foreach ($table2[$i] as $onerecord) {
                        if (false === empty($onerecord)) {
                            $record = new Record();
                            $record->setCountry($country);
                            $record->setMcc(strip_tags($onerecord[0]));
                            $record->setMnc(strip_tags($onerecord[1]));
                            $record->setNetwork(strip_tags($onerecord[2]));
                            $record->setOperatorOrBrand(strip_tags($onerecord[3]));
                            $record->setStatus(strip_tags($onerecord[4]));
                            $this->save($record);
                        }
                    }
                }
            }
        }
        $response = new Response(json_encode('success'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param $object
     *
     * @return mixed
     */
    public function save($object)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($object);
        $entityManager->flush();

        return $object;
    }

}
